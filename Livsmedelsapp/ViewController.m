//
//  ViewController.m
//  Livsmedelsapp
//
//  Created by Ole Fritsch on 13/03/16.
//  Copyright © 2016 Ole Fritsch. All rights reserved.
//

#import "ViewController.h"
#import "TableViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *logo;
@property (weak, nonatomic) IBOutlet UITextField *searchField;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet UIButton *aboutButton;
@property (weak, nonatomic) IBOutlet UIButton *favoriteButton;
@property (nonatomic) NSArray *data;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view, typically from a nib.
    self.searchField.delegate = self;
    
    self.searchField.alpha = 0;
    self.searchButton.alpha = 0;
    self.aboutButton.alpha = 0;
    self.favoriteButton.alpha = 0;
    

    [UIView animateWithDuration:1.5 animations:^{
        CGRect frame = self.logo.frame;
        frame.origin.y = self.view.frame.origin.y / 2;
        self.logo.frame = frame;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:1.0 animations:^{
            self.searchField.alpha = 1;
            self.searchButton.alpha = 1;
            self.aboutButton.alpha = 1;
            self.favoriteButton.alpha = 1;
        } completion:nil];
    }];

}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)search:(UIButton *)sender {
    NSString *urlString = [NSString stringWithFormat:@"http://matapi.se/foodstuff?query=%@&format=json", self.searchField.text];
    NSString *encoded = [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    NSURL *url = [NSURL URLWithString:encoded];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *_Nullable data, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"Error: %@", error);
            return;
        }
        
        NSError *parseError = nil;
        NSArray *result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
        
        if (parseError) {
            NSLog(@"Failed to parse data: %@", parseError);
            return;
        }
        
        self.data = [result mutableCopy];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self performSegueWithIdentifier:@"Search" sender:self];
        });
        
    }];
    
    [task resume];

}

- (IBAction)favorites:(UIButton *)sender {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.data = [defaults arrayForKey:@"favorites"];
    
    [self performSegueWithIdentifier:@"Favorites" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"Search"] || [[segue identifier] isEqualToString:@"Favorites"]) {
        TableViewController *destination = [segue destinationViewController];
        destination.data = [self.data mutableCopy];
    }
}

@end
