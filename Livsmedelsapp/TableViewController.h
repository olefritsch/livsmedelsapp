//
//  TableViewController.h
//  Livsmedelsapp
//
//  Created by Ole Fritsch on 13/03/16.
//  Copyright © 2016 Ole Fritsch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewController : UITableViewController <UISearchResultsUpdating>

@property (nonatomic) NSMutableArray *data;
@property (nonatomic) NSArray *firstFoodForComparing;
@property (nonatomic) bool isPresentedModallyForSecondComparision;

@end
