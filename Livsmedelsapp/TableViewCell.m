//
//  TableViewCell.m
//  Livsmedelsapp
//
//  Created by Ole on 15/03/16.
//  Copyright © 2016 Ole Fritsch. All rights reserved.
//

#import "TableViewCell.h"

@implementation TableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
