//
//  InformationViewController.m
//  Livsmedelsapp
//
//  Created by Ole Fritsch on 22/03/16.
//  Copyright © 2016 Ole Fritsch. All rights reserved.
//

#import "InformationViewController.h"
#import "TableViewController.h"

@interface InformationViewController ()

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *kcal;
@property (weak, nonatomic) IBOutlet UILabel *protein;
@property (weak, nonatomic) IBOutlet UILabel *fett;
@property (weak, nonatomic) IBOutlet UILabel *fiber;
@property (weak, nonatomic) IBOutlet UILabel *vitaminB6;
@property (weak, nonatomic) IBOutlet UILabel *vitaminB12;
@property (weak, nonatomic) IBOutlet UILabel *vitaminC;
@property (weak, nonatomic) IBOutlet UILabel *vitaminD;
@property (weak, nonatomic) IBOutlet UILabel *vitaminE;
@property (weak, nonatomic) IBOutlet UILabel *vitaminK;
@property (weak, nonatomic) IBOutlet UILabel *healthyLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end

@implementation InformationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.activityIndicator.center = self.view.center;
    [self.activityIndicator startAnimating];
    [self.activityIndicator setHidesWhenStopped:YES];
    
    UIImage *cachedImage = [UIImage imageWithContentsOfFile:[self imagePath]];
    if (cachedImage) {
        self.imageView.image = cachedImage;
    }
    
    [self setData];
    [self.activityIndicator stopAnimating];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setData {
    
    NSNumber *energyKcal = self.nutrientValues[@"energyKcal"];
    NSNumber *protein = self.nutrientValues[@"protein"];
    NSNumber *fat = self.nutrientValues[@"fat"];
    NSNumber *carbohydrates = self.nutrientValues[@"carbohydrates"];
    
    NSNumber *vitaminB6 = self.nutrientValues[@"vitaminB6"];
    NSNumber *vitaminB12 = self.nutrientValues[@"vitaminB12"];
    NSNumber *vitaminC = self.nutrientValues[@"vitaminC"];
    NSNumber *vitaminD = self.nutrientValues[@"vitaminD"];
    NSNumber *vitaminE = self.nutrientValues[@"vitaminE"];
    NSNumber *vitaminK = self.nutrientValues[@"vitaminK"];
    
    int vitaminValue = ((vitaminB6.intValue + vitaminB12.intValue + vitaminC.intValue + vitaminD.intValue + vitaminE.intValue + vitaminK.intValue) * 10);
    int healthValue = (protein.intValue / fat.intValue) + (carbohydrates.intValue / 2) + vitaminValue;
    
    self.kcal.text = [NSString stringWithFormat:@"%@ kcal", energyKcal];
    self.protein.text = [NSString stringWithFormat:@"%@ g", protein];
    self.fett.text = [NSString stringWithFormat:@"%@ g", fat];
    self.fiber.text = [NSString stringWithFormat:@"%@ g", carbohydrates];
    self.vitaminB6.text = [NSString stringWithFormat:@"%@ g", vitaminB6];
    self.vitaminB12.text = [NSString stringWithFormat:@"%@ g", vitaminB12];
    self.vitaminC.text = [NSString stringWithFormat:@"%@ g", vitaminC];
    self.vitaminD.text = [NSString stringWithFormat:@"%@ g", vitaminD];
    self.vitaminE.text = [NSString stringWithFormat:@"%@ g", vitaminE];
    self.vitaminK.text = [NSString stringWithFormat:@"%@ g", vitaminK];
    self.healthyLabel.text = [NSString stringWithFormat:@"Nyttighetsvärdet: %d", healthValue];
    
    
    
    /*
    NSString *urlString = [NSString stringWithFormat:@"http://matapi.se/foodstuff/%@", self.queryNumber];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *_Nullable data, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"Error: %@", error);
            return;
        }
        
        NSError *parseError = nil;
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
        
        if (parseError) {
            NSLog(@"Failed to parse data: %@", parseError);
            return;
        }
        
        NSDictionary *nutrientValues = result[@"nutrientValues"];
        
        NSNumber *energyKcal = nutrientValues[@"energyKcal"];
        NSNumber *protein = nutrientValues[@"protein"];
        NSNumber *fat = nutrientValues[@"fat"];
        NSNumber *carbohydrates = nutrientValues[@"carbohydrates"];
        
        NSNumber *vitaminB6 = nutrientValues[@"vitaminB6"];
        NSNumber *vitaminB12 = nutrientValues[@"vitaminB12"];
        NSNumber *vitaminC = nutrientValues[@"vitaminC"];
        NSNumber *vitaminD = nutrientValues[@"vitaminD"];
        NSNumber *vitaminE = nutrientValues[@"vitaminE"];
        NSNumber *vitaminK = nutrientValues[@"vitaminK"];
        
        int vitaminValue = ((vitaminB6.intValue + vitaminB12.intValue + vitaminC.intValue + vitaminD.intValue + vitaminE.intValue + vitaminK.intValue) * 10);
        int healthValue = (protein.intValue / fat.intValue) + (carbohydrates.intValue / 2) + vitaminValue;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            self.kcal.text = [NSString stringWithFormat:@"%@ kcal", energyKcal];
            self.protein.text = [NSString stringWithFormat:@"%@ g", protein];
            self.fett.text = [NSString stringWithFormat:@"%@ g", fat];
            self.fiber.text = [NSString stringWithFormat:@"%@ g", carbohydrates];
            self.vitaminB6.text = [NSString stringWithFormat:@"%@ g", vitaminB6];
            self.vitaminB12.text = [NSString stringWithFormat:@"%@ g", vitaminB12];
            self.vitaminC.text = [NSString stringWithFormat:@"%@ g", vitaminC];
            self.vitaminD.text = [NSString stringWithFormat:@"%@ g", vitaminD];
            self.vitaminE.text = [NSString stringWithFormat:@"%@ g", vitaminE];
            self.vitaminK.text = [NSString stringWithFormat:@"%@ g", vitaminK];
            self.healthyLabel.text = [NSString stringWithFormat:@"Nyttighetsvärdet: %d", healthValue];
          
            [self.activityIndicator stopAnimating];
            
        });
    }];
    
    [task resume];
    */
    
}

- (IBAction)takePhoto:(UIBarButtonItem *)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    } else if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum]) {
        picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    }
    
    [self presentViewController:picker animated:YES completion:nil];
}



- (void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(nonnull NSDictionary<NSString *,id> *)info {
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    UIImage *image = info[UIImagePickerControllerEditedImage];
    
    self.imageView.image = image;
    
    NSData *imageData = UIImagePNGRepresentation(image);
    BOOL success = [imageData writeToFile:self.imagePath atomically:YES];
    
    if (success) {
        NSLog(@"Saved image to user documents directory.");
    } else {
        NSLog(@"Failed to save image.");
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (NSString*)imagePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = paths[0];
    NSString *pathSuffix = [[[self.title stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"%" withString:@""] stringByAppendingString:@".png"];
    return [path stringByAppendingPathComponent:pathSuffix];
}

- (IBAction)favorite:(UIButton *)sender {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *favorites = [[defaults arrayForKey:@"favorites"] mutableCopy];
    
    NSMutableDictionary *foodstuff = [[NSMutableDictionary alloc] init];
    [foodstuff setObject:self.title forKey:@"name"];
    [foodstuff setObject:self.queryNumber forKey:@"number"];
    [foodstuff setObject:self.kcal.text forKey:@"energyKcal"];
    
    if (!favorites) {
        favorites = [[NSMutableArray alloc] init];
    }
    
    [favorites addObject:foodstuff];
    [defaults setObject:favorites forKey:@"favorites"];
    [defaults synchronize];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Added to Favorites" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {}];

    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)compare:(UIButton *)sender {
    NSLog(@"Compare");
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"Compare"]) {
        TableViewController *destination = [segue destinationViewController];
        NSArray *data = @[self.title, @[@"Kcal", self.nutrientValues[@"energyKcal"]], @[@"Protein", self.nutrientValues[@"protein"]], @[@"Fett", self.nutrientValues[@"fat"]], @[@"Carbs", self.nutrientValues[@"carbohydrates"]]];
        destination.firstFoodForComparing = data;
        destination.isPresentedModallyForSecondComparision = YES;
    }
}


@end
