//
//  TableViewController.m
//  Livsmedelsapp
//
//  Created by Ole Fritsch on 13/03/16.
//  Copyright © 2016 Ole Fritsch. All rights reserved.
//

#import "TableViewController.h"
#import "TableViewCell.h"
#import "InformationViewController.h"
#import "ComparisonViewController.h"

@interface TableViewController ()

@property (nonatomic) UISearchController *searchController;
@property (nonatomic) NSArray *searchResult;
@property (nonatomic) NSString *foodstuff;
@property (nonatomic) NSDictionary *nutrientValues;
@property (nonatomic) NSArray *activeData;

@end

@implementation TableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!self.data) {
        [self getData];
    }
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.definesPresentationContext = YES;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.tableView.tableHeaderView = self.searchController.searchBar;
    
    self.tableView.contentOffset = CGPointMake(0, self.searchController.searchBar.frame.size.height);
    self.searchController.searchBar.barTintColor = [UIColor colorWithRed:140 green:189 blue:64 alpha:1];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    NSString *searchText = searchController.searchBar.text;
    
    NSPredicate *findFoodWithName = [NSPredicate predicateWithFormat:@"name contains[c] %@", searchText];
    self.searchResult = [self.data filteredArrayUsingPredicate:findFoodWithName];
    
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.searchController.isActive && self.searchController.searchBar.text.length > 0) {
        return self.searchResult.count;
    } else {
        return self.data.count;
    }
}


- (TableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    int index = (int)indexPath.row;
    
    if (self.searchController.isActive && self.searchController.searchBar.text.length > 0) {
        self.activeData = self.searchResult;
    } else {
        self.activeData = self.data;
    }
    
    cell.nameLabel.text = self.activeData[index][@"name"];
    
    if (self.activeData[index][@"nutrientValues"]) {
        cell.energyLabel.text = [NSString stringWithFormat:@"%@ kcal", self.activeData[index][@"nutrientValues"][@"energyKcal"]];
    } else {
        [self getDataForCell:cell withIndex:index];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.isPresentedModallyForSecondComparision) {
        self.nutrientValues = self.data[indexPath.row][@"nutrientValues"];
        
        [self performSegueWithIdentifier:@"Compare" sender:[self.tableView cellForRowAtIndexPath:indexPath]];
    } else {
        [self performSegueWithIdentifier:@"Detail" sender:[self.tableView cellForRowAtIndexPath:indexPath]];
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    TableViewCell *cell = sender;
    
    if ([[segue identifier] isEqualToString:@"Detail"]) {

        InformationViewController *destination = [segue destinationViewController];

        int senderRow = (int)[self.tableView indexPathForCell:cell].row;
        
        destination.nutrientValues = [self.data[senderRow][@"nutrientValues"] mutableCopy];
        destination.queryNumber = self.data[senderRow][@"number"];
        destination.title = cell.nameLabel.text;
        
    } else if ([[segue identifier] isEqualToString:@"Compare"]) {
        
        ComparisonViewController *destination = [segue destinationViewController];
        NSArray *secondFoodForComparing = @[cell.nameLabel.text, @[@"Kcal", self.nutrientValues[@"energyKcal"]], @[@"Protein", self.nutrientValues[@"protein"]], @[@"Fett", self.nutrientValues[@"fat"]], @[@"Carbs", self.nutrientValues[@"carbohydrates"]]];
        NSMutableArray *compareData = [[NSMutableArray alloc] init];

        for (int i = 0; i<4; i++) {
            [compareData addObject:self.firstFoodForComparing[i]];
            [compareData addObject:secondFoodForComparing[i]];
        }
        
        destination.data = compareData;
    }
}


#pragma mark - JSON

- (void)getDataForCell:(TableViewCell*)cell withIndex:(int)index {
    NSString *urlString = [NSString stringWithFormat:@"http://matapi.se/foodstuff/%@", self.data[index][@"number"]];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *_Nullable data, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"Error: %@", error);
            return;
        }
        
        NSError *parseError = nil;
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
        
        if (parseError) {
            NSLog(@"Failed to parse data: %@", parseError);
            return;
        }

        dispatch_async(dispatch_get_main_queue(), ^{
            NSDictionary *nutrientValues = result[@"nutrientValues"];
            NSNumber *kcal = nutrientValues[@"energyKcal"];
            cell.energyLabel.text = [NSString stringWithFormat:@"%@ kcal", kcal];
            
            NSMutableDictionary *newDict = [self.data[index] mutableCopy];
            [newDict setObject:nutrientValues forKey:@"nutrientValues"];
            [self.data replaceObjectAtIndex:index withObject:newDict];
        });
        
    }];
    
    [task resume];
    
}

- (void)getData {
    NSString *urlString = @"http://matapi.se/foodstuff";
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *_Nullable data, NSURLResponse *response, NSError *error) {
        
        if (error) {
            NSLog(@"Error: %@", error);
            return;
        }
        
        NSError *parseError = nil;
        NSArray *result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
        
        if (parseError) {
            NSLog(@"Failed to parse data: %@", parseError);
            return;
        }
        
        self.data = [result mutableCopy];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
        
    }];
    
    [task resume];
}


@end
