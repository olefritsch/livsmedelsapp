//
//  AboutViewController.m
//  Livsmedelsapp
//
//  Created by Ole Fritsch on 01/04/16.
//  Copyright © 2016 Ole Fritsch. All rights reserved.
//

#import "AboutViewController.h"

@interface AboutViewController ()


@property (weak, nonatomic) IBOutlet UIView *version;
@property (weak, nonatomic) IBOutlet UIView *copyright;
@property (weak, nonatomic) IBOutlet UIView *year;

@property (nonatomic) UIDynamicAnimator *animator;
@property (nonatomic) UIGravityBehavior *gravity;
@property (nonatomic) UICollisionBehavior *collision;

@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGAffineTransform transform1 = CGAffineTransformMakeRotation(M_PI / 10);
    CGAffineTransform transform2 = CGAffineTransformMakeRotation(M_PI / 18);
    CGAffineTransform transform3 = CGAffineTransformMakeRotation(M_PI / 14);
    self.version.transform = transform1;
    self.copyright.transform = transform2;
    self.year.transform = transform3;
    
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    self.gravity = [[UIGravityBehavior alloc] initWithItems:@[self.version, self.copyright, self.year]];
    self.collision = [[UICollisionBehavior alloc] initWithItems:@[self.version, self.copyright, self.year]];
    self.collision.translatesReferenceBoundsIntoBoundary = YES;
    
    [self.animator addBehavior:self.gravity];
    [self.animator addBehavior:self.collision];
    
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0f];
    CGAffineTransform newTransform1 = CGAffineTransformMakeRotation(0.8);
    CGAffineTransform newTransform2 = CGAffineTransformMakeRotation(-0.6);
    CGAffineTransform newTransform3 = CGAffineTransformMakeRotation(0.7);
    self.version.transform = newTransform1;
    self.copyright.transform = newTransform2;
    self.year.transform = newTransform3;
    [UIView commitAnimations];
    
}



@end
