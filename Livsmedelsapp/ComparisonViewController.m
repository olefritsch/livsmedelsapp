//
//  ComparisonViewController.m
//  Livsmedelsapp
//
//  Created by Ole Fritsch on 01/04/16.
//  Copyright © 2016 Ole Fritsch. All rights reserved.
//

#import "ComparisonViewController.h"
#import "TableViewController.h"
#import <GKBarGraph.h>

@interface ComparisonViewController ()

@property (weak, nonatomic) IBOutlet GKBarGraph *graph;
@property (weak, nonatomic) IBOutlet UILabel *firstFoodNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondFoodNameLabel;

@end

@implementation ComparisonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.firstFoodNameLabel.text = self.data[0];
    self.secondFoodNameLabel.text = self.data[1];
    
    self.graph.dataSource = self;
    self.graph.backgroundColor = [UIColor colorWithRed:255 green:255 blue:255 alpha:0.5];
    [self.graph draw];
}

- (NSInteger)numberOfBars {
    return 6;
}

- (NSNumber *)valueForBarAtIndex:(NSInteger)index {
    
    if (index < 2) {
        return @([self.data[index + 2][1] intValue] / 10);
    } else if (index < 4) {
        return @([self.data[index + 2][1] intValue] * 10);
    } else {
        return self.data[index + 2][1];
    }
}

- (NSString *)titleForBarAtIndex:(NSInteger)index {
    return self.data[index + 2][0];
}

- (UIColor *)colorForBarAtIndex:(NSInteger)index {
    return @[[UIColor yellowColor], [UIColor orangeColor]][index % 2];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
}


@end
