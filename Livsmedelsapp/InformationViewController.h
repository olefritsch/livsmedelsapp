//
//  InformationViewController.h
//  Livsmedelsapp
//
//  Created by Ole Fritsch on 22/03/16.
//  Copyright © 2016 Ole Fritsch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InformationViewController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (nonatomic) NSString *name;
@property (nonatomic) NSNumber *queryNumber;
@property (nonatomic) NSMutableDictionary *nutrientValues;

@end
