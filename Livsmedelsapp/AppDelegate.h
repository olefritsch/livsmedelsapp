//
//  AppDelegate.h
//  Livsmedelsapp
//
//  Created by Ole Fritsch on 13/03/16.
//  Copyright © 2016 Ole Fritsch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

